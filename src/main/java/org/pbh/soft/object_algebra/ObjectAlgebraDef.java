package org.pbh.soft.object_algebra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.function.Function;

import static java.lang.String.format;

/**
 * Definition an Object Algebra.  Typical Usage is <pre>
 *   Define the data types
 *
 *   {@code public interface Tree {}
 *
 *     public class Fork implements Tree {...}
 *     public class Leaf implements Tree {...}
 *   }
 *
 *   Define the Object Algebra definition for the data types
 *
 *   {@code public interface TreeObjectAlgebraDef<R> extends ObjectAlgebraDef<Tree, R> {
 *      R visit(final Fork fork);
 *      R visit(final Leaf leaf);
 *   }
 *   }
 *
 *   To implement a recursive implementation of the size of the tree an Operation is implemented as follows
 *
 *   {@code public class SizeOp implements TreeObjectAlgebraDef<Supplier<Integer>> {
 *       Supplier<Integer> visit(final Fork fork) {
 *         final SizeOp self = this;
 *         return () -> 1 + self.apply(fork.getLeft()).get() + self.apply(fork.getRight()).get();
 *       }
 *
 *       Supplier<Integer> visit(final Leaf leaf) {
 *         final SizeOp self = this;
 *         return () -> 1;
 *       }
 *     }
 *   }
 *
 *   Adding a data type is straightforward: Add the data type definition and extend the Object Algebra definition
 *
 *   {@code public class Fork3 implements Tree {...}
 *
 *     public interface ExtendedTreeObjectAlgebraDef<R> extends TreeObjectAlgebraDef<R> {
 *       R visit(final Fork3 fork);
 *     }
 *   }
 *
 *   Adding the data type to an existing operation
 *
 *   {@code public class ExtendedSizeOp extends SizeOp implements ExtendedTreeObjectAlgebraDef<R> {
 *       Supplier<Integer> visit(final Fork3 fork) {
 *         final SizeOp self = this;
 *         return () -> 1 + self.apply(fork.getLeft()).get() + self.apply(fork.getMiddle()).get() + self.apply(fork.getRight()).get();
 *       }
 *     }
 *   }
 * </pre>
 * @param <RootType> type of the common interface for this ObjectAlgebra.  If there is not a root type for this ObjectAlgebra
 *                   consider using {@link NoRootType} as the <i>RootType</i>
 * @param <R> result type for this ObjectAlgebra
 */
public interface ObjectAlgebraDef<RootType, R> extends Function<RootType, R> {
  Logger LOGGER = LoggerFactory.getLogger(ObjectAlgebraDef.class);

  /**
   * @return the name of the '<i>visit</i>' methods defined in the Object Algebra definition.  The default
   * implementation of this method returns {@code "visit"}
   */
  default String visitMethodName() {
    return "visit";
  }

  /**
   *
   * @param r instance of a subtype of {@code RootType}
   * @return default result for {@code r}
   */
  R visitDefault(final RootType r);

  /**
   *
   * @param r instance of a subtype of {@code RootType}
   * @return if {@code r} is <i>null</i> returns <i>null</i> otherwise uses reflection in an attempt to
   * find the correct method in the Object Algebra to invoke using the type of {@code r}.  The result of invoking the found
   * method is returned.  If the correct method is not found then the result of calling {@link #visitDefault(Object)} is
   * returned
   */
  @Override
  default R apply(final RootType r) {
    if (r == null) {
      LOGGER.debug("supplied argument null!  Not able to dispatch by type therefore returning null.");
      return null;
    }

    final String methodName = visitMethodName();
    final Class<?> rClass = r.getClass();
    try {
      @SuppressWarnings("unchecked")
      final R result = (R) this.getClass().getMethod(methodName, rClass).invoke(this, r);
      return result;
    } catch (final NoSuchMethodException e) {
      LOGGER.warn(format("Unable to find the method with name: %s with argument of type: %s", methodName, rClass.getName()), e);
      return visitDefault(r);
    } catch (final InvocationTargetException | IllegalAccessException e) {
      LOGGER.error(format("Unable to invoke method with name: %s with argument of type: %s", methodName, rClass.getName()), e);
      throw new ObjectAlgebraApplyException(e);
    }
  }
}
