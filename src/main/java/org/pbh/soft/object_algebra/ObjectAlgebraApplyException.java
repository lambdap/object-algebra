package org.pbh.soft.object_algebra;

/**
 * Represents an {@link Exception} when invoking the {@link ObjectAlgebraDef#apply(Object)} method
 */
public class ObjectAlgebraApplyException extends RuntimeException {
  public ObjectAlgebraApplyException(final Throwable cause) {
    super(cause);
  }
}
