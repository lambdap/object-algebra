package org.pbh.soft.object_algebra.labyrinth.cases;

import java.util.Objects;

public class Passage implements Node {
  private final int length;

  public Passage(final int length) {
    this.length = length;
  }

  public static Passage passage(final int length) {
    return new Passage(length);
  }

  public int getLength() {
    return length;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final Passage passage = (Passage) o;
    return Objects.equals(length, passage.length);
  }

  @Override
  public int hashCode() {
    return Objects.hash(length);
  }

  @Override
  public String toString() {
    return "Passage{" +
      "length=" + length +
      '}';
  }
}
