package org.pbh.soft.object_algebra.labyrinth;

import org.pbh.soft.object_algebra.labyrinth.algebra.DrawLabyrinth;
import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.pbh.soft.object_algebra.labyrinth.cases.Fork;
import org.junit.Test;

import static org.pbh.soft.object_algebra.labyrinth.cases.Passage.passage;

public class DrawLabyrinthTest {
  private final DrawLabyrinth draw = new DrawLabyrinth();

  @Test
  public void testPassage() throws Exception {
    draw.visit(passage(4)).accept(0);
  }

  @Test
  public void testDeadEnd() throws Exception {
    draw.visit(DeadEnd.INSTANCE).accept(0);
  }

  @Test
  public void testFork() throws Exception {
    draw.visit(Fork.fork(passage(3), DeadEnd.INSTANCE)).accept(0);
  }
}
