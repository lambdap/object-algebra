package org.pbh.soft.object_algebra.labyrinth;

import org.pbh.soft.object_algebra.labyrinth.algebra.NodeObjectAlgebraDef;
import org.pbh.soft.object_algebra.labyrinth.algebra.PPrintLabyrinth;
import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.pbh.soft.object_algebra.labyrinth.cases.Node;
import org.junit.Test;

import java.util.Objects;
import java.util.function.Consumer;

import static org.pbh.soft.object_algebra.labyrinth.cases.Fork.fork;
import static org.pbh.soft.object_algebra.labyrinth.cases.Passage.passage;

public class ExtendedTypeTest {

  public static class Fork3 implements Node {
    private final Node left;
    private final Node middle;
    private final Node right;

    public Fork3(final Node left, final Node middle, final Node right) {
      this.left = left;
      this.middle = middle;
      this.right = right;
    }

    public static Fork3 fork3(final Node left, final Node middle, final Node right) {
      return new Fork3(left, middle, right);
    }

    public Node getLeft() {
      return left;
    }

    public Node getMiddle() {
      return middle;
    }

    public Node getRight() {
      return right;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      final Fork3 fork3 = (Fork3) o;
      return Objects.equals(left, fork3.left) &&
        Objects.equals(middle, fork3.middle) &&
        Objects.equals(right, fork3.right);
    }

    @Override
    public int hashCode() {
      return Objects.hash(left, middle, right);
    }

    @Override
    public String toString() {
      return "Fork3{" +
        "left=" + left +
        ", middle=" + middle +
        ", right=" + right +
        '}';
    }
  }

  public interface NodeObjectAlgebraDefExtended<R> extends NodeObjectAlgebraDef<R> {
    R visit(final Fork3 fork3);
  }

  public static class PPrintLabyrinthExtended extends PPrintLabyrinth implements NodeObjectAlgebraDefExtended<Consumer<Integer>> {
    @Override
    public Consumer<Integer> visit(final Fork3 fork3) {
      final PPrintLabyrinthExtended self = this;
      return indentAmt -> {
        System.out.println(indent(indentAmt) + "fork3");
        self.apply(fork3.getLeft()).accept(indentAmt + 1);
        self.apply(fork3.getMiddle()).accept(indentAmt + 1);
        self.apply(fork3.getRight()).accept(indentAmt + 1);
      };
    }
  }

  private final PPrintLabyrinthExtended pprint = new PPrintLabyrinthExtended();

  @Test
  public void testPassage() throws Exception {
    pprint.visit(passage(4)).accept(0);
  }

  @Test
  public void testDeadEnd() throws Exception {
    pprint.visit(DeadEnd.INSTANCE).accept(0);
  }

  @Test
  public void testFork() throws Exception {
    pprint.visit(fork(passage(3), DeadEnd.INSTANCE)).accept(0);
  }

  @Test
  public void testFork3() throws Exception {
    pprint.visit(Fork3.fork3(passage(3), passage(2), DeadEnd.INSTANCE)).accept(0);
  }

  @Test
  public void testFork_complex() throws Exception {
    pprint.visit(fork(passage(3), Fork3.fork3(fork(DeadEnd.INSTANCE, DeadEnd.INSTANCE), DeadEnd.INSTANCE, passage(4)))).accept(0);
  }
}
