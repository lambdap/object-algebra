package org.pbh.soft.object_algebra.labyrinth.cases;

import java.util.Objects;

public class Fork implements Node {
  private final Node left;
  private final Node right;

  public Fork(final Node left, final Node right) {
    this.left = left;
    this.right = right;
  }

  public static Fork fork(final Node left, final Node right) {
    return new Fork(left, right);
  }

  public Node getLeft() {
    return left;
  }

  public Node getRight() {
    return right;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final Fork fork = (Fork) o;
    return Objects.equals(left, fork.left) &&
      Objects.equals(right, fork.right);
  }

  @Override
  public int hashCode() {
    return Objects.hash(left, right);
  }

  @Override
  public String toString() {
    return "Fork{" +
      "left=" + left +
      ", right=" + right +
      '}';
  }
}
