package org.pbh.soft.object_algebra.labyrinth;

import org.pbh.soft.object_algebra.labyrinth.algebra.NodeObjectAlgebraDef;
import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.pbh.soft.object_algebra.labyrinth.cases.Fork;
import org.pbh.soft.object_algebra.labyrinth.cases.Passage;
import org.junit.Test;

import java.util.function.Supplier;

import static org.pbh.soft.object_algebra.labyrinth.cases.Fork.fork;
import static org.pbh.soft.object_algebra.labyrinth.cases.Passage.passage;
import static org.junit.Assert.assertEquals;

public class ExtendedOperationTest {
  public static class LabyrinthLength implements NodeObjectAlgebraDef<Supplier<Integer>> {
    @Override
    public Supplier<Integer> visit(final DeadEnd deadEnd) {
      return () -> 0;
    }

    @Override
    public Supplier<Integer> visit(final Passage passage) {
      return passage::getLength;
    }

    @Override
    public Supplier<Integer> visit(final Fork fork) {
      final LabyrinthLength self = this;
      return () -> self.apply(fork.getLeft()).get() + self.apply(fork.getRight()).get();
    }
  }

  private final LabyrinthLength op = new LabyrinthLength();

  @Test
  public void testDeadEnd() throws Exception {
    assertEquals((Integer) 0, op.visit(DeadEnd.INSTANCE).get());
  }

  @Test
  public void testPassage() throws Exception {
    assertEquals((Integer) 4, op.visit(passage(4)).get());
  }

  @Test
  public void testFork() throws Exception {
    assertEquals((Integer) 6, op.visit(fork(passage(3), fork(passage(2), passage(1)))).get());
  }
}
