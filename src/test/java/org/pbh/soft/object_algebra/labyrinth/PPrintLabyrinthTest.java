package org.pbh.soft.object_algebra.labyrinth;

import org.pbh.soft.object_algebra.labyrinth.algebra.PPrintLabyrinth;
import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.junit.Test;

import static org.pbh.soft.object_algebra.labyrinth.cases.Fork.fork;
import static org.pbh.soft.object_algebra.labyrinth.cases.Passage.passage;

public class PPrintLabyrinthTest {
  private final PPrintLabyrinth pprint = new PPrintLabyrinth();

  @Test
  public void testPassage() throws Exception {
    pprint.visit(passage(4)).accept(0);
  }

  @Test
  public void testDeadEnd() throws Exception {
    pprint.visit(DeadEnd.INSTANCE).accept(0);
  }

  @Test
  public void testFork() throws Exception {
    pprint.visit(fork(passage(3), DeadEnd.INSTANCE)).accept(0);
  }

  @Test
  public void testFork_complex() throws Exception {
    pprint.visit(fork(passage(3), fork(fork(DeadEnd.INSTANCE, DeadEnd.INSTANCE), passage(4)))).accept(0);
  }
}
