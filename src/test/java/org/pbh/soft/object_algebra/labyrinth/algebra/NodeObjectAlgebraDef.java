package org.pbh.soft.object_algebra.labyrinth.algebra;

import org.pbh.soft.object_algebra.ObjectAlgebraDef;
import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.pbh.soft.object_algebra.labyrinth.cases.Fork;
import org.pbh.soft.object_algebra.labyrinth.cases.Node;
import org.pbh.soft.object_algebra.labyrinth.cases.Passage;

public interface NodeObjectAlgebraDef<R> extends ObjectAlgebraDef<Node, R> {
  R visit(final DeadEnd deadEnd);
  R visit(final Passage passage);
  R visit(final Fork fork);

  @Override
  default R visitDefault(final Node r) {
    throw new UnsupportedOperationException("All cases are known, this should never happen!?");
  }
}
