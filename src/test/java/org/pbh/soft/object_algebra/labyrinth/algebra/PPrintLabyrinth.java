package org.pbh.soft.object_algebra.labyrinth.algebra;

import org.pbh.soft.object_algebra.labyrinth.cases.DeadEnd;
import org.pbh.soft.object_algebra.labyrinth.cases.Fork;
import org.pbh.soft.object_algebra.labyrinth.cases.Passage;

import java.util.Collections;
import java.util.function.Consumer;

public class PPrintLabyrinth implements NodeObjectAlgebraDef<Consumer<Integer>> {
  @Override
  public Consumer<Integer> visit(final DeadEnd deadEnd) {
    return indentAmt -> System.out.println(indent(indentAmt) + "deadend");
  }

  @Override
  public Consumer<Integer> visit(final Passage passage) {
    return indentAmt -> System.out.println(indent(indentAmt) + "passage(" + passage.getLength() + ")");
  }

  @Override
  public Consumer<Integer> visit(final Fork fork) {
    final PPrintLabyrinth self = this;
    return indentAmt -> {
      System.out.println(indent(indentAmt) + "fork");
      self.apply(fork.getLeft()).accept(indentAmt + 1);
      self.apply(fork.getRight()).accept(indentAmt + 1);
    };
  }

  protected String indent(final int indentAmt) {
    return String.join("", Collections.nCopies(indentAmt, "  "));
  }
}
