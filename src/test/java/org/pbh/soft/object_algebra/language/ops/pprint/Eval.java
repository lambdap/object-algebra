package org.pbh.soft.object_algebra.language.ops.pprint;


import org.pbh.soft.object_algebra.language.cases.Int;

import java.util.function.Supplier;

/**
 * Marker interface for the evaluation of expressions
 */
public interface Eval extends Supplier<Int> {

}
