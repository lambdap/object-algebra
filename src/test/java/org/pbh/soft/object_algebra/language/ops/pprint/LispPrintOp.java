package org.pbh.soft.object_algebra.language.ops.pprint;

import org.pbh.soft.object_algebra.language.LanguageObjAlgDef;
import org.pbh.soft.object_algebra.language.cases.Add;
import org.pbh.soft.object_algebra.language.cases.Int;
import org.pbh.soft.object_algebra.language.cases.Mul;

import static java.lang.String.format;

public class LispPrintOp implements LanguageObjAlgDef<PPrint> {
  @Override
  public PPrint visit(final Int num) {
    return () -> Integer.toString(num.getValue());
  }

  @Override
  public PPrint visit(final Add add) {
    final LispPrintOp self = this;
    return () -> format("(+ %s %s)", self.apply(add.getX()).get(), self.apply(add.getY()).get());
  }

  @Override
  public PPrint visit(final Mul mul) {
    final LispPrintOp self = this;
    return () -> format("(* %s %s)", self.apply(mul.getX()).get(), self.apply(mul.getY()).get());
  }
}