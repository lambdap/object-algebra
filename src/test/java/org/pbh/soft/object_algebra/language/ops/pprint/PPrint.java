package org.pbh.soft.object_algebra.language.ops.pprint;

import java.util.function.Supplier;

/**
 * Marker interface for the pretty print operation on expressions
 */
public interface PPrint extends Supplier<String> {
}
