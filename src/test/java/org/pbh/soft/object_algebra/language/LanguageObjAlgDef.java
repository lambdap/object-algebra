package org.pbh.soft.object_algebra.language;

import org.pbh.soft.object_algebra.ObjectAlgebraDef;
import org.pbh.soft.object_algebra.language.cases.Add;
import org.pbh.soft.object_algebra.language.cases.Exp;
import org.pbh.soft.object_algebra.language.cases.Int;
import org.pbh.soft.object_algebra.language.cases.Mul;

public interface LanguageObjAlgDef<R> extends ObjectAlgebraDef<Exp, R> {
  R visit(final Int num);
  R visit(final Add add);
  R visit(final Mul mul);

  @Override
  default R visitDefault(final Exp r) {
    if (r == null) {
      throw new NullPointerException("Exp to visitDefault was null!");
    }
    throw new UnsupportedOperationException("Exp with type: " + r.getClass() + " not supported!");
  }
}