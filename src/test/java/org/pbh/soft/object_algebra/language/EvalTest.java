package org.pbh.soft.object_algebra.language;


import org.pbh.soft.object_algebra.language.ops.pprint.Eval;
import org.pbh.soft.object_algebra.language.ops.pprint.EvalOp;
import org.junit.Test;

import static org.pbh.soft.object_algebra.language.cases.Language.add;
import static org.pbh.soft.object_algebra.language.cases.Language.lint;
import static org.pbh.soft.object_algebra.language.cases.Language.mul;
import static org.junit.Assert.assertEquals;

public class EvalTest {
  final LanguageObjAlgDef<Eval> eval = new EvalOp();

  @Test
  public void testLispPPrint_int() throws Exception {
    assertEquals(lint(3), eval.visit(lint(3)).get());
  }

  @Test
  public void testLispPPrint_plus() throws Exception {
    assertEquals(lint(3), eval.visit(add(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_mul() throws Exception {
    assertEquals(lint(2), eval.visit(mul(lint(2), lint(1))).get());
  }

  @Test
  public void testLispPPrint_complex() throws Exception {
    assertEquals(lint(5), eval.visit(add(lint(2), mul(lint(3), lint(1)))).get());
  }
}