package org.pbh.soft.object_algebra.language.cases;

import java.util.Objects;

public class Mul implements Exp {
  private final Exp x;
  private final Exp y;

  public Mul(final Exp x, final Exp y) {
    this.x = x;
    this.y = y;
  }

  public Exp getX() {
    return x;
  }

  public Exp getY() {
    return y;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final Mul add = (Mul) o;
    return Objects.equals(x, add.x) &&
      Objects.equals(y, add.y);
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public String toString() {
    return "Add{" +
      "x=" + x +
      ", y=" + y +
      '}';
  }
}
